from math import log, sqrt
import numpy as np
from scipy.optimize import curve_fit


class GameModel(object):
    def __init__(self):
        #Speed impulse response assumed in the form V = AB * Exp(-Bt)
        #A is usually 10, ie constant throttle of 1.0 will cause V to tend to 10
        self.a = 10
        self.b = 0.02
        
        #Model assumes speed and rotation to be separate
        self.maxFricTorque = 0
        self.maxAng = 0
        
        #values to be used figuring out model
        self.vels = list()
        
    def distToSpeed(self, v1, v2):
        #Returns the distance needed to (acc/de)cellerate from v1 to v2
        if v1>self.a: v1=self.a-0.0001
        elif v1<0: v1=0
        if v2>self.a: v2=self.a-0.0001
        elif v2<0: v2=0
        
        if v1 > v2:
            #for dccn, dist = (v_1-v_2)/b
            return (v1-v2)/self.b
        elif v2 > v1:
            #for accn, dist = (a (log(1-v_1/a)-log(1-v_2/a))+v_1-v_2)/b
            temp1 = log(1.0-v1/self.a)
            temp2 = log(1.0-v2/self.a)
            return -(self.a*(temp1-temp2)+v1-v2)/self.b
        else:
            return 0

    def calcSpeedModel(self, log):
        #work out self.a and self.b from self.vels
        
        speeds = log[:,0]
        t = np.arange(len(speeds))
        popt, pcov = curve_fit(self.accn_model, t, speeds)
        
        self.a = popt[0]
        self.b = popt[1]
        
        print "Speed model parameters found: A={0}, B={1}".format(self.a, self.b)
        
    def apply_turbo(self, turbo):
        self.a = self.a * turbo.factor
        
    def remove_turbo(self, turbo):
        self.a = self.a / turbo.factor
        
    def accn_model(self, t, a, b, offset):
        #assumes full throttle
        return a*(1-np.exp(-b*t + offset))
