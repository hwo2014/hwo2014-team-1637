from math import pi
import json
from operator import itemgetter
import numpy as np
import os

class raceTrack:
    pieces = list()

    @classmethod
    def populate(cls, data):
        #expects 'data' to be the entire data payload from gameInit
        
        lanes = data['race']['track']['lanes']
        pieces = data['race']['track']['pieces']
        
        cls.lanes = list()
        for row in lanes:
            cls.lanes.append({"index" : row['index'], 
                              "DfromC": row['distanceFromCenter']})
            pass
        
        #There's no gaurentee that the lanes will be sent in the right order
        cls.lanes.sort(key=itemgetter("index"))

        for row in data['race']['track']['pieces']:
            piece = row
            if 'length' in row:
                piece['straight'] = True
                piece['radius'] = float("inf")
                length = row['length']
                piece['length'] = list()
                for lane in cls.lanes:
                    piece['length'].append(length)
            else:
                piece['straight'] = False
                piece['length'] = list()
                for lane in cls.lanes:
                    laneLength = abs( (pi/180) * (piece['radius']+lane['DfromC']) * piece['angle'])
                    piece['length'].append(laneLength)
                
            if 'switch' in row:
                piece['switch'] = bool(row['switch'])
            else:
                piece['switch'] = False
            cls.pieces.append(piece)
        cls.name = data['race']['track']['id']
        #cls.laplength = sum([row['length'][0] for row in cls.pieces])
        
        if not os.path.isfile(os.path.join("tracks", cls.name + ".track")):
            with open(os.path.join("tracks", cls.name + ".track"), 'w') as f:
                f.writelines(json.dumps(cls.pieces))


    @classmethod
    def posToFloat(cls, data):
        #expects the piecePosition object from carpositions
        #returns total distance: ie, beginning of lap2 != 0
        index = data['pieceIndex']
        pos = sum([row['length'][0] for row in cls.pieces[0:index]])
        pos += data['inPieceDistance']
        #pos += cls.laplength * data['lap']
        return pos
        
    @classmethod
    def nextCorner(cls, data):
        #expects the piecePosition object from carpositions
        curIndex = data['pieceIndex']
        curLane = data['lane']['startLaneIndex']
        nextIndex = curIndex + 1
        if nextIndex == len(cls.pieces):
            nextIndex = 0
            
        if cls.pieces[curIndex]['straight']:
            distance = cls.pieces[curIndex]['length'][curLane] - data['inPieceDistance'] 
            return distance, cls.pieces[nextIndex]
        else:
            return 0, cls.pieces[curIndex]
            
class car:
    def __init__(self, data):
        self.id = data['id']
        self.dimensions = data['dimensions']
        
        self.lastFloatPos = 0
        self.floatPos = 0 #total distance around the track
        self.speed = 0
        
        self.lastPos = 0
        self.pos = 0
        
        self.lastAng = 0
        self.ang = 0
        
        self.lastAngVel = 0
        self.angVel = 0
        
        self.angAccn = 0
        
        #speed, radius, angle
        self.log = np.array([[0, 0, 0]])
        
    def __eq__(self, other):
        return other == self.id
        
    def update(self, data):
        self.lastFloatPos = self.floatPos
        self.floatPos = raceTrack.posToFloat(data['piecePosition'])

        self.lastPos = self.pos
        self.pos = data['piecePosition']
        
        newSpeed = self.floatPos-self.lastFloatPos
        if abs(newSpeed - self.speed) < 2:
            self.speed = newSpeed #speed in 'units/tick'...

        self.lastAng = self.ang
        self.ang =  data['angle']

        self.lastAngVel = self.angVel
        self.angVel = self.ang - self.lastAng
        
        self.angAccn = self.angVel - self.lastAngVel
        
        
class turbo(object):
    def __init__(self, data):
        self.duration_ms = data['turboDurationMilliseconds']
        self.duration_tks = data['turboDurationTicks']
        self.factor = data['turboFactor']

