import json
from states import car, raceTrack
from basebot import BaseBot
from model import *

class TragicBot(BaseBot):
    def __init__(self, s, name, key):
        super(TragicBot, self).__init__(s, name, key)

        self.desiredSpeed = 10
        self.switching = False
        
        self.slowing = False
        
        self.investigating = True
        self.model = GameModel() #blank model of the games physics
        self.gameLog = np.array([[0, 0, 0, 0]])
        
    def on_game_init(self, data):
        super(TragicBot, self).on_game_init(data)

    def on_car_positions(self, data):
        self.processPositions(data)
        
        if self.investigating:
            self.investigate()
            return

        #find next corner
        dist, piece = raceTrack.nextCorner(self.us.pos)

        #magic switchMap for keimola
        switchMap = { 2: "Right",
                      4: "Left",
                      12: "Left",
                      17: "Right" }
        
        if self.us.pos['pieceIndex'] in switchMap:
            if not self.switching:
                self.switching = True
                #self.switchLane(switchMap[raw_us['piecePosition']['pieceIndex']])
        else:
            self.switching = False


        cornerSpeeds = { 50 : 4,
                         100: 6.5,
                         200: 10 }

        if self.slowing:
            if piece['radius'] not in cornerSpeeds and dist != 0:
                self.slowing = False
                self.desiredSpeed = 100
            else:
                self.desiredSpeed = cornerSpeeds[piece['radius']]
        elif piece['radius'] in cornerSpeeds:
            if dist < self.model.distToSpeed(self.us.speed, cornerSpeeds[piece['radius']]):
                self.slowing = True
                self.desiredSpeed = cornerSpeeds[piece['radius']]
        else:
            self.desiredSpeed = 100
        
        if abs(self.us.ang) > 50 and sign(self.us.angVel) is sign(self.us.ang):
            self.desiredSpeed -= 0.3
            
            
        self.setThrottle()
        
        if self.log:
            self.logRow()
        
    def setThrottle(self):
        #controls the throttle to achieve self.desiredSpeed ASAP
        error = self.desiredSpeed - self.us.speed
        throttle = self.us.speed/10 + 2*error
        
        if self.desiredSpeed == 0:
            throttle = 0
        #keep the throttle within bounds
        if throttle < 0: throttle = 0
        if throttle > 1: throttle = 1

        self.throttle(throttle)
        
    def investigate(self):
        #probe the track to find model parameters

        if self.gameTick > 10:
            self.model.calcSpeedModel(self.gameLog)
            self.investigating = False
        else:
            self.throttle(1.0)
        
        if self.log:
            self.logRow()
        
        self.setThrottle()
    
    def processPositions(self, data):
        for row in data:
            if row['id'] == self.us:
                self.us.update(row)
            else:
                pass
                
        speed = self.us.speed
        ang = self.us.ang
        angVel = self.us.angVel
        radius = raceTrack.pieces[self.us.pos['pieceIndex']]['radius']
        if speed > 0:
            self.gameLog = np.append(self.gameLog, [[speed, ang, angVel, radius]], axis=0)

    def on_turbo_start(self, data):
        super(TragicBot, self).on_turbo_start(data)
        self.model.apply_turbo(self.turbo)
    def on_turbo_end(self, data):
        super(TragicBot, self).on_turbo_end(data)
        self.model.remove_turbo(self.turbo)

def sign(x):
    if x>0:
        return 1
    elif x < 0:
        return -1
    else:
        return x
