import json
from states import *
import csv
from os import remove
from os.path import isfile

class BaseBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.botName = self.__class__.__name__
        
        self.turboAvailable = False
        self.gameTick = 0
        self.opponents = list()
        
        with open('config') as f:
            config = json.load(f)
        for thing in config:
            setattr(self, thing, config[thing])

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def createRace(self):
        msgType = "createRace"
        data  = {"botId": {"name": self.name,
                           "key" : self.key}}
        data["trackName"] =  "germany"
        data["carCount"] = 2
        data['password'] = "32857"
        return self.msg(msgType, data)
        
    def joinRace(self):
        msgType = "joinRace"
        data  = {"botId": {"name": self.name,
                           "key" : self.key}}
        if self.desiredTrack:
            data["trackName"] =  self.desiredTrack
        data["carCount"] = self.desiredCarNum
        return self.msg(msgType, data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})
        
    def switchLane(self, data):
        self.msg("switchLane", data)

    def run(self):
        if self.desiredTrack:
            self.joinRace()
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()
    
    def on_join_race(self, data):
        print("Joined race")
        self.ping()
    
    def on_your_car(self, data):
        print("Received our car")
        #colors are gaurenteed to be unique, names are not
        self.us = data['color']
    
    def on_game_init(self, data):
        print("Game initializing")
        
        if not len(raceTrack.pieces):
            raceTrack.populate(data)
        
        #populate list of cars racing
        if not len(self.opponents):
            for row in data['race']['cars']:
                if row['id']['color'] == self.us:
                    self.us = car(row)
                else:
                    self.opponents.append(car(row))
                
        #setup race info
        if 'laps'in data['race']['raceSession']:
            self.totalLaps = data['race']['raceSession']['laps']
            self.mode = 'race'
        else:
            self.totalLaps = None
            self.qTime = data['race']['raceSession']['durationMs']
            self.mode = 'qualifying'
        
        #print some info
        opNum = len(self.opponents)
        if not opNum:
            opNum = "no"
        color = self.us.id['color'].lower()
        if color == "red":
            color += " (vroom vroom)"
        
        if self.totalLaps:
            print("We're doing {0} laps of {1} with {2} opponents".format(
                  self.totalLaps, raceTrack.name.capitalize(), opNum))
        else:
            print ("We're doing {0}ms of qualifying of {1} with {2} opponents".format(
                   self.qTime, raceTrack.name.capitalize(), opNum))
        print("We're an instance of {0}, and our car is {1}".format(
              self.botName, color))
                
                
                
        if self.log:
            if isfile("racelog-complete.csv"):
                remove("racelog-complete.csv")
                
            with open("racelog-complete.csv", 'a') as f:
                logger = csv.writer(f, delimiter=',')
                logger.writerow(["pos", "speed", "ang", "angVel", "angAccn", "radius"])

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for row in data:
            if row['id'] == self.us:
                self.us.update(row)
                raw_us = row
            else:
                pass
        self.throttle(0.5)
        if self.logging:
            self.logRow()
    
    def on_turbo_available(self, data):
        self.turbo = turbo(data)
        self.turboAvailable = True
        print "got a turbo of {0}x for {1} ticks".format(
                                     self.turbo.factor, self.turbo.duration_tks)
                                     
    def on_turbo_start(self, data):
        print "Turbo period started, vroom vroom"
        
    def on_turbo_end(self, data):
        print "Turbo over :("
    
    def fire_turbo(self):
        self.turboAvailable = False
        self.msg("turbo", "boom-shaka-loom")

    def on_lap_finished(self, data):
        if data['car']==self.us:
            lapNum = data['lapTime']['lap']
            time = float(data['lapTime']['millis'])/1000
            ticks = data['lapTime']['ticks']
            if self.totalLaps:
                print("Lap {0}/{1} Finished: ".format(lapNum+1, self.totalLaps))
            else:
                print "Qualifying lap finished"
            print("Time: {0} ({1} ticks)".format(time, ticks))

    def on_crash(self, data):
        print("Someone crashed, what a tit")
        crashLog = dict()
		
        if data == self.us:
            print("Bugger, it was us")
			
            #Dynamics info
            crashLog['speed'] = self.us.speed
            crashLog['desiredSpeed'] = self.desiredSpeed
            crashLog['angle'] = self.us.ang
            crashLog['angVel'] = self.us.angVel
            crashLog['angAccn'] = self.us.angAccn
            crashLog['piecePosition'] = self.us.pos
			
            #Track info
            pos = self.us.pos
            pieceIndex = pos['pieceIndex']
            crashLog['piece'] = raceTrack.pieces[pieceIndex]
			
            with open('crashes.log', 'a') as f:
                f.write(json.dumps(crashLog))
                f.write("\n")
        else:
            print("Thank god, it was the other guy ({0})".format(data['name']))
            
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()
        
    def on_tournament_end(self, data):
        print("Tournament ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            
            'lapFinished': self.on_lap_finished,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
                
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
            line = socket_file.readline()
            
    def logRow(self):
        with open("racelog-complete.csv", 'a') as f:
            logger = csv.writer(f, delimiter=',')
            loglist = list()
            loglist += [self.us.floatPos, self.us.speed, self.us.ang, self.us.angVel, self.us.angAccn]
            piece = raceTrack.pieces[self.us.pos['pieceIndex']]
            if 'radius' in piece:
                loglist += [piece['radius']]
            else:
                loglist += [float("inf")]
            logger.writerow(loglist)
